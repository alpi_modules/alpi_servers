var basis = require('alpi_basis');

module.exports = {
    /**
     * @summary Check if the server package is installed.
     * @param {Function} callback 
     */
    IsServerInstalled: function (callback) {
        basis.System.PackageManager((packageManager) => {
            if (packageManager === "pacman" || packageManager === "apt") {
                basis.System.IsPackageInstalled("vsftpd", (installed) => {
                    callback(installed);
                });
            } else {
                //it is assumed it is installed
                callback(true);
            }
        });
    },
    /**
     * @summary Install the server package.
     * @param {Function} callback 
     */
    InstallServer: function (callback) {
        basis.System.InstallPackage("vsftpd", (installed) => {
            callback(installed === true);
        });
    },
    /**
     * @summary Show vsftpd version.
     * @param {Function} callback 
     */
    Version: function (callback) {
        module.exports.IsServerInstalled((installed) => {
            if (installed === true) {
                basis.System.ShellCommand("vsftpd -v", (version) => {
                    if (version.error === null) {
                        callback(version.stdout.trim());
                    } else {
                        callback(false);
                    }
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * @summary FTP service management.
     */
    Service: require('./ftp/service'),
    /**
     * @summary FTP configuration.
     */
    Configuration: require('./ftp/configuration')
};