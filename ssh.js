var basis = require('alpi_basis');

module.exports = {
    /**
     * @summary Check if the server package is installed.
     * @param {Function} callback 
     */
    IsServerInstalled: function (callback) {
        basis.System.PackageManager((packageManager) => {
            if (packageManager === "pacman") {
                basis.System.IsPackageInstalled("openssh", (installed) => {
                    callback(installed);
                });
            } else if (packageManager === "apt") {
                basis.System.IsPackageInstalled("openssh-server", (installed) => {
                    callback(installed);
                });
            } else {
                //it is assumed it is installed
                callback(true);
            }
        });
    },
    /**
     * @summary Install the server package.
     * @param {Function} callback 
     */
    InstallServer: function (callback) {
        basis.System.PackageManager((packageManager) => {
            if (packageManager === "pacman") {
                basis.System.InstallPackage("openssh", (installed) => {
                    callback(installed === true);
                });
            } else if (packageManager === "apt") {
                basis.System.InstallPackage("openssh-server", (installed) => {
                    callback(installed === true);
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * @summary Show openssh version.
     * @param {Function} callback 
     */
    Version: function (callback) {
        module.exports.IsServerInstalled((installed) => {
            if (installed === true) {
                basis.System.ShellCommand("ssh -V", (version) => {
                    if (version.error === null) {
                        callback(version.stdout.trim());
                    } else {
                        callback(false);
                    }
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * @summary SSH service management.
     */
    Service: require('./ssh/service'),
    /**
     * @summary SSH configuration.
     */
    Configuration: require('./ssh/configuration')
};