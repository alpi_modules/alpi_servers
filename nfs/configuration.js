var basis = require('alpi_basis');
var files = require('alpi_files');
var users = require('alpi_users');
var filters = require('alpi_filters');

module.exports = {
    /**
     * @summary Check and change permissions of the /etc/exports file.
     * @param {Function} callback
     */
    SetupCheck: function (callback) {
        require('../nfs').IsServerInstalled(installed => {
            if (installed === true) {
                if (files.IsFile("/etc/exports") === true) {
                    files.FileInformations("/etc/exports", (informations) => {
                        if (informations !== false) {
                            new Promise((resolve, reject) => {
                                //check if the group "admin_nfs" exists
                                users.Groups.Exists("admin_nfs", (exists) => {
                                    if (exists === false) {
                                        //create it if it isn't
                                        users.Groups.Create("admin_nfs", (e) => {
                                            resolve(e);
                                        });
                                    } else {
                                        resolve(true);
                                    }
                                });
                            }).then((result) => {
                                return new Promise((resolve, reject) => {
                                    if (result === true) {
                                        //get the current user
                                        basis.System.Whoami((user) => {
                                            if (user !== false) {
                                                //check if it belong to the group "admin_nfs"
                                                users.Groups.UserBelongToGroup(user, 'admin_nfs', (belongto) => {
                                                    if (belongto === true) {
                                                        resolve(true);
                                                    } else {
                                                        //add the secondary group "admin_nfs" to the current user
                                                        users.Groups.AddUserSecondaryGroupWithoutPassword(user, "admin_nfs", (added) => {
                                                            resolve(added);
                                                        });
                                                    }
                                                });
                                            } else {
                                                resolve(false);
                                            }
                                        });
                                    } else {
                                        resolve(false);
                                    }
                                });
                            }).then((result) => {
                                return new Promise((resolve, reject) => {
                                    if (result === true) {
                                        //check owner
                                        if (informations.owner + ":" + informations.group !== "root:admin_nfs") {
                                            basis.System.ShellCommand("sudo chown root:admin_nfs /etc/exports", (e) => {
                                                resolve(e.error === null);
                                            });
                                        } else {
                                            resolve(true);
                                        }
                                    } else {
                                        resolve(false);
                                    }
                                });
                            }).then(result => {
                                if (result === true) {
                                    //check rights
                                    if (informations.rights !== "-rw-rw-r--") {
                                        basis.System.ShellCommand("sudo chmod 664 /etc/exports", (e) => {
                                            callback(e.error === null);
                                        });
                                    } else {
                                        callback(true);
                                    }
                                } else {
                                    callback(false);
                                }
                            });
                        } else {
                            callback(false);
                        }
                    });
                } else {
                    basis.System.ShellCommand("sudo touch /etc/exports", (e) => {
                        if (e.error === null) {
                            module.exports.SetupCheck(e => {
                                callback(e);
                            });
                        } else {
                            callback(false);
                        }
                    });
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * @summary List available nfs shares.
     * @param {Function} callback 
     */
    ListShares: function (callback) {
        module.exports.SetupCheck(check => {
            if (check === true) {
                basis.System.ShellCommand("cat /etc/exports | sed '/^#/ d'", (e) => {
                    if (e.error === null) {
                        var informations = e.stdout.trim().split('\n');
                        var shares = [];
                        if (informations.length > 0) {
                            informations.forEach(share => {
                                share = share.split('(');
                                share[0] = share[0].substring(share[0].lastIndexOf(' ') + 1);
                                share[1] = share[1].split(',');
                                shares.push({
                                    folder: share[0][0],
                                    ip: share[0][0],
                                    options: share[1]
                                });
                            });
                        }
                        callback(shares);
                    } else {
                        callback(false);
                    }
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * @summary Add a new nfs share.
     * @param {String} folder 
     * @param {String} ip 
     * @param {Array} options 
     * @param {Function} callback 
     */
    AddShare: function (folder, ip, options, callback) {
        folder = filters.Escape.Spaces(filters.Other.NormalizePath(folder));
        module.exports.SetupCheck(check => {
            if (check === true) {
                if (files.IsDirectory(folder) === true) {
                    var optionsOk = true;
                    options.forEach(option => {
                        if (module.exports.OptionExists(option) === false) {
                            optionsOk = false;
                        }
                    });
                    if (optionsOk === true) {
                        if (filters.Validate.IpV4(ip) === true || filters.Validate.IpV6(ip) === true) {
                            //@ts-ignore
                            options = options.join(',');
                            basis.System.ShellCommand("echo '" + folder + " " + ip + "(" + options + ")' >> /etc/exports", (e) => {
                                callback(e.error === null);
                            });
                        } else {
                            callback(false);
                        }
                    } else {
                        callback(false);
                    }
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * @summary Remove an existing nfs share.
     * @param {String} folder 
     * @param {String} ip 
     * @param {Function} callback 
     */
    RemoveShare: function (folder, ip, callback) {
        folder = filters.Escape.Spaces(filters.Other.NormalizePath(folder));
        module.exports.SetupCheck(check => {
            if (check === true) {
                if (files.IsDirectory(folder) === true) {
                    if (filters.Validate.IpV4(ip) === true || filters.Validate.IpV6(ip) === true) {
                        basis.System.ShellCommand("sed -i '/" + folder + " " + ip + "/d' /etc/exports", (e) => {
                            callback(e.error === null);
                        });
                    } else {
                        callback(false);
                    }
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * @summary Returns an array of all available nfs options.
     * @returns {Array}
     */
    ListOptions: function () {
        return (['rw', 'async', 'sync', 'root_squash', 'no_root_squash', 'all_squash', 'anonuid', 'anongid', 'subtree_check', 'no_subtree_check']);
    },
    /**
     * @summary Checks if a specific option exists.
     * @param {String} option 
     * @returns {Boolean}
     */
    OptionExists: function (option) {
        if (option.includes('=')) {
            option = option.split('=')[0];
            return option == "anonuid" || option == "anongid";
        } else {
            return module.exports.ListOptions().includes(option);
        }
    }
};