var services = require('alpi_services');

module.exports = {
    Status: function (callback) {
        services.Status("nfs-server", (status) => {
            callback(status);
        });
    },
    Start: function (callback) {
        services.Start("nfs-server", (started) => {
            callback(started);
        });
    },
    Stop: function (callback) {
        services.Stop("nfs-server", (stopped) => {
            callback(stopped);
        });
    },
    BootStatus: function (callback) {
        services.BootStatus("nfs-server", (bootStatus) => {
            callback(bootStatus);
        });
    },
    Enable: function (callback) {
        services.Enable("nfs-server", (enabled) => {
            callback(enabled);
        });
    },
    Disable: function (callback) {
        services.Disable("nfs-server", (disabled) => {
            callback(disabled);
        });
    }
};