var services = require('alpi_services');

module.exports = {
    Status: function (callback) {
        services.Status("smbd", (status) => {
            callback(status);
        });
    },
    Start: function (callback) {
        services.Start("smbd", (started) => {
            callback(started);
        });
    },
    Stop: function (callback) {
        services.Stop("smbd", (stopped) => {
            callback(stopped);
        });
    },
    BootStatus: function (callback) {
        services.BootStatus("smbd", (bootStatus) => {
            callback(bootStatus);
        });
    },
    Enable: function (callback) {
        services.Enable("smbd", (enabled) => {
            callback(enabled);
        });
    },
    Disable: function (callback) {
        services.Disable("smbd", (disabled) => {
            callback(disabled);
        });
    }
};