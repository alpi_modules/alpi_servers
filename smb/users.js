var basis = require('alpi_basis');
var users = require('alpi_users');
var filters = require('alpi_filters');

module.exports = {
    /**
     * @summary List existing smb users.
     * @param {Function} callback 
     */
    List: function (callback) {
        basis.System.ShellCommand("sudo pdbedit -L | cut -d':' -f1", (e) => {
            if (e.error === null) {
                callback(e.stdout.trim().split('\n'));
            } else {
                callback(false);
            }
        });
    },
    /**
     * @summary Check if the username in parameter is a smb user.
     * @param {String} username 
     * @param {Function} callback 
     */
    Exists: function (username, callback) {
        if (filters.Validate.Username(username) === true) {
            module.exports.List(users => {
                callback(users.includes(username));
            });
        } else {
            callback(false);
        }
    },
    /**
     * @summary Add new smb user.
     * @param {String} username 
     * @param {String} password 
     * @param {Function} callback 
     */
    Add: function (username, password, callback) {
        password = require('alpi_filters').Escape.Quotes(password);
        users.Users.Exists(username, (exists) => {
            if (exists === true) {
                module.exports.Exists(username, (exists) => {
                    if (exists === false) {
                        basis.System.ShellCommand("(echo '" + password + "'; sleep 1; echo '" + password + "') | sudo smbpasswd -s -a " + username, (e) => {
                            callback(e.error === null);
                        });
                    } else {
                        callback(false);
                    }
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * @summary Change the password of an existing smb user, root permissions not needed but old password required.
     * @param {String} username 
     * @param {String} oldPassword 
     * @param {String} newPassword 
     * @param {Function} callback 
     */
    ChangePassword: function (username, oldPassword, newPassword, callback) {
        module.exports.Exists(username, (exists) => {
            if (exists === true) {
                basis.System.ShellCommand("(echo '" + oldPassword + "'; sleep 1; echo '" + newPassword + "'; sleep 1; echo '" + newPassword + "') | smbpasswd -U " + username, (e) => {
                    callback(e.stderr === null);
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * @summary Change the password of an existing smb user, root permissions needed but old password not required.
     * @param {String} username 
     * @param {String} password 
     * @param {Function} callback 
     */
    ChangePasswordWithoutOldPassword: function (username, password, callback) {
        module.exports.Exists(username, (exists) => {
            if (exists === true) {
                basis.System.ShellCommand("(echo '" + password + "'; sleep 1; echo '" + password + "') | smbpasswd -U " + username, (e) => {
                    callback(e.stderr === null);
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * @summary Disable the selected user.
     * @param {String} username 
     * @param {Function} callback 
     */
    Disable: function (username, callback) {
        module.exports.Exists(username, (exists) => {
            if (exists === true) {
                basis.System.ShellCommand("sudo smbpassword -d -U " + username, (e) => {
                    callback(e.error === null);
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * @summary Enable the selected user.
     * @param {String} username 
     * @param {Function} callback 
     */
    Enable: function (username, callback) {
        module.exports.Exists(username, (exists) => {
            if (exists === true) {
                basis.System.ShellCommand("sudo smbpassword -e -U " + username, (e) => {
                    callback(e.error === null);
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * @summary Remove the selected user.
     * @param {String} username 
     * @param {Function} callback 
     */
    Remove: function (username, callback) {
        users.Users.Exists(username, (exists) => {
            if (exists === true) {
                basis.System.ShellCommand("sudo smbpass -x -U " + username, (e) => {
                    callback(e.error === null);
                });
            } else {
                callback(false);
            }
        });
    }
}