var basis = require('alpi_basis');
var users = require('alpi_users');
var files = require('alpi_files');

module.exports = {

    ////////////////////// https://jlk.fjfi.cvut.cz/arch/manpages/man/smb.conf.5 //////////////////////

    /**
     * @summary Check and change permissions of the smbd configuration file.
     * @param {Function} callback
     */
    SetupCheck: function (callback) {
        require('../smb').IsServerInstalled((installed) => {
            if (installed === true) {
                if (files.IsFile("/etc/samba/smb.conf") === true) {
                    files.FileInformations("/etc/samba/smb.conf", (informations) => {
                        if (informations !== false) {
                            new Promise((resolve, reject) => {
                                //check if the group "admin_smb" exists
                                users.Groups.Exists("admin_smb", (exists) => {
                                    if (exists === false) {
                                        //create it if it isn't
                                        users.Groups.Create("admin_smb", (e) => {
                                            resolve(e);
                                        });
                                    } else {
                                        resolve(true);
                                    }
                                });
                            }).then((result) => {
                                return new Promise((resolve, reject) => {
                                    if (result === true) {
                                        //get the current user
                                        basis.System.Whoami((user) => {
                                            if (user !== false) {
                                                //check if it belong to the group "admin_smb"
                                                users.Groups.UserBelongToGroup(user, 'admin_smb', (belongto) => {
                                                    if (belongto === true) {
                                                        resolve(true);
                                                    } else {
                                                        //add the secondary group "admin_smb" to the current user
                                                        users.Groups.AddUserSecondaryGroupWithoutPassword(user, "admin_smb", (added) => {
                                                            resolve(added);
                                                        });
                                                    }
                                                });
                                            } else {
                                                resolve(false);
                                            }
                                        });
                                    } else {
                                        resolve(false);
                                    }
                                });
                            }).then((result) => {
                                return new Promise((resolve, reject) => {
                                    if (result === true) {
                                        //check owner
                                        if (informations.owner + ":" + informations.group !== "root:admin_smb") {
                                            basis.System.ShellCommand("sudo chown root:admin_smb /etc/samba/smb.conf", (e) => {
                                                resolve(e.stderr === "");
                                            });
                                        } else {
                                            resolve(true);
                                        }
                                    } else {
                                        resolve(false);
                                    }
                                });
                            }).then(result => {
                                if (result === true) {
                                    //check rights
                                    if (informations.rights !== "-rw-rw-r--") {
                                        basis.System.ShellCommand("sudo chmod 664 /etc/samba/smb.conf", (e) => {
                                            callback(e.stderr === "");
                                        });
                                    } else {
                                        callback(true);
                                    }
                                } else {
                                    callback(false);
                                }
                            });
                        } else {
                            callback(false);
                        }
                    });
                } else {
                    basis.System.ShellCommand("sudo touch /etc/samba/smb.conf", (e) => {
                        if (e.stderr === "") {
                            module.exports.SetupCheck((e) => {
                                callback(e);
                            });
                        } else {
                            callback(false);
                        }
                    });
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * @summary Check if the configuration file is right.
     * @param {Function} callback 
     */
    Check: function (callback) {
        module.exports.SetupCheck(check => {
            if (check === true) {
                basis.System.ShellCommand("testparm -s", e => {
                    if (e.error === null) {
                        callback(true);
                    } else {
                        callback(false);
                    }
                });
            }
        });
    },
    /**
     * @summary Return a json array containing default smbd configuration.
     * @param {Function} callback 
     */
    GetDefault: function (callback) {
        module.exports.SetupCheck(check => {
            if (check === true) {
                basis.System.ShellCommand("testparm -sv /tmp  | grep -v '#\\|copy =\\|winbind separator =\\|client schannel =\\|client use spnego principal =\\|enable privileges =\\|idmap backend =\\|idmap gid =\\|idmap uid =\\|ldap ssl ads =\\|lsa over netlogon =\\|nbt client socket address =\\|null passwords =\\|server schannel =\\|syslog =\\|syslog only =\\|unicode =\\|acl check permissions =\\|client schannel =\\|client use spnego principal =\\|enable privileges =\\|idmap backend =\\|idmap gid =\\|idmap uid =\\|ldap ssl ads =\\|lsa over netlogon =\\|nbt client socket address =\\|null passwords =\\|server schannel =\\|syslog =\\|syslog only =\\|unicode =\\|acl check permissions ='", (config) => {
                    if (config.error === null) {
                        callback(ConfigurationParser(config));
                    } else {
                        callback(false);
                    }
                });
            } else {
                callback(false);
            }
        });
    },
    /** 
     * @summary Return a json array containing the current smbd configuration.
     * @param {Function} callback
     */
    GetCurrent: function (callback) {
        module.exports.SetupCheck(check => {
            if (check === true) {
                basis.System.ShellCommand("testparm -sv | grep -v '#\\|copy =\\|winbind separator =\\|client schannel =\\|client use spnego principal =\\|enable privileges =\\|idmap backend =\\|idmap gid =\\|idmap uid =\\|ldap ssl ads =\\|lsa over netlogon =\\|nbt client socket address =\\|null passwords =\\|server schannel =\\|syslog =\\|syslog only =\\|unicode =\\|acl check permissions =\\|client schannel =\\|client use spnego principal =\\|enable privileges =\\|idmap backend =\\|idmap gid =\\|idmap uid =\\|ldap ssl ads =\\|lsa over netlogon =\\|nbt client socket address =\\|null passwords =\\|server schannel =\\|syslog =\\|syslog only =\\|unicode =\\|acl check permissions ='", (config) => {
                    if (config.error === null) {
                        callback(ConfigurationParser(config));
                    } else {
                        callback(false);
                    }
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * @summary Set the configuration in parameter.
     * @param {Object} config the configuration can be retrieve using the function GetCurrent and GetDefault.
     * @param {Function} callback 
     */
    Set: function (config, callback) {
        module.exports.SetupCheck((check) => {
            if (check === true) {
                var result = "";
                Object.keys(config).forEach(section => {
                    result += section + "\n";
                    Object.keys(config[section]).forEach(setting => {
                        result += setting + " = " + config[section][setting] + "\n";
                    });
                });
                files.OverwriteFileContent("/etc/samba/smb.conf", result, (e) => {
                    callback(e);
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * @summary Reset the configuration to default.
     * @param {Function} callback 
     */
    Reset: function (callback) {
        module.exports.GetDefault(defaultConfig => {
            if (defaultConfig !== false) {
                module.exports.Set(defaultConfig, e => {
                    callback(e);
                });
            } else {
                callback(false);
            }
        });
    }
};

/**
 * @summary Function used to parse the configuration outputted by the testparm command.
 * @param {Object} config
 * @returns {Object}
 */
function ConfigurationParser(config) {
    config = config.stdout.trim().split("\n");
    var result = {};
    var section = null;
    config.forEach(line => {
        if (line.startsWith("[")) {
            section = line;
            result[section] = {};
        } else {
            line = line.trim().split("=");
            var setting = line[0];
            var value = line[1];
            result[section][setting] = value;
        }
    });
    return result;
}