var files = require('alpi_files');
var users = require('alpi_users');
var filters = require('alpi_filters');

module.exports = {
    /**
     * Reset the specified setting to his default value.
     * @param {String} settingName
     * @param {Function} callback
     */
    Default: function (settingName, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                if (config[settingName] !== undefined) {
                    delete config[settingName];
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * Specifies what environment variables sent by the client will be copied into the session's environ. See SendEnv in ssh_config for how to configure the client. The TERM environment variable is always sent whenever the client requests a pseudo-terminal as it is required by the protocol. Variables are specified by name, which may contain the wildcard characters ‘*’ and ‘?’. Multiple environment variables may be separated by whitespace or spread across multiple AcceptEnv directives. Be warned that some environment variables could be used to bypass restricted user environments. For this reason, care should be taken in the use of this directive. The default is not to accept any environment variables.
     * @param {String} env
     * @param {Function} callback
     */
    AcceptEnv: function (env, callback) {
        console.warn("There's no verification in this function");
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                config.acceptenv = env;
                require('./configuration').Set(config, (written) => {
                    callback(written);
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * Specifies which address family should be used by sshd(8). Valid arguments are any (the default), inet (use IPv4 only), or inet6 (use IPv6 only).
     * @summary Specifies which address family should be used.
     * @param {String} addressFamily - 'any': ipV4 & ipV6, 'inet': ipV4 only, 'inet6': ipV6 only. Default: any
     * @param {Function} callback
     */
    AddressFamily: function (addressFamily, callback) {
        if (addressFamily === "any" || addressFamily === "inet" || addressFamily === "inet6") {
            require('./configuration').GetCurrent((config) => {
                if (config !== false) {
                    config.addressfamily = addressFamily;
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            });
        } else {
            callback(false);
        }
    },
    /**
     * Specifies whether ssh-agent forwarding is permitted. The default is yes. Note that disabling agent forwarding does not improve security unless users are also denied shell access, as they can always install their own forwarders.
     * @summary Specifies whether ssh-agent forwarding is permitted.
     * @param {Boolean} allow - 'true': ssh agent forwarding is permitted, 'false' it isn't.
     * @param {Function} callback
     */
    AllowAgentForwarding: function (allow, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                if (typeof allow === "boolean") {
                    if (allow === true) {
                        config.allowagentforwarding = "yes";
                    } else if (allow === false) {
                        config.allowagentforwarding = "no";
                    }
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * This keyword can be followed by a list of group name patterns, separated by spaces. If specified, login is allowed only for users whose primary group or supplementary group list matches one of the patterns. Only group names are valid; a numerical group ID is not recognized. By default, login is allowed for all groups. The allow/deny directives are processed in the following order: DenyUsers, AllowUsers, DenyGroups, and finally AllowGroups.
     * @summary Allow the specified groups to login.
     * @param {String[]} groups - List of group.Default: empty (login is allowed for all groups).
     * @param {Function} callback
     */
    AllowGroups: function (groups, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                users.Groups.Exists(groups, (e) => {
                    var exists = true;
                    e.forEach(group => {
                        if (e[group] === false) {
                            exists = false;
                        }
                    });
                    if (exists === true) {
                        // @ts-ignore
                        groups.push({
                            separator: " "
                        });
                        config.allowgroups = groups;
                        require('./configuration').Set(config, (written) => {
                            callback(written);
                        });
                    } else {
                        callback(false);
                    }
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * Specifies whether StreamLocal (Unix-domain socket) forwarding is permitted. The available options are yes (the default) or all to allow StreamLocal forwarding, no to prevent all StreamLocal forwarding, local to allow local (from the perspective of ssh) forwarding only or remote to allow remote forwarding only. Note that disabling StreamLocal forwarding does not improve security unless users are also denied shell access, as they can always install their own forwarders.
     * @summary Specifies whether StreamLocal (Unix-domain socket) forwarding is permitted.
     * @param {String} allow - 'yes' == 'all': Allow StreamLocal forwarding, 'no': Prevent all StreamLocal forwarding, 'local': Allow local forwarding only, 'remote': Allow remote forwarding only.Default: 'yes'
     * @param {Function} callback
     */
    AllowStreamLocalForwarding: function (allow, callback) {
        if (allow === "yes" || allow === "all" || allow === "no" || allow === "local" || allow === "remote") {
            require('./configuration').GetCurrent((config) => {
                if (config !== false) {
                    config.allowstreamlocalforwarding = allow;
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            });
        } else {
            callback(false);
        }
    },
    /**
     * Specifies whether TCP forwarding is permitted. The available options are yes (the default) or all to allow TCP forwarding, no to prevent all TCP forwarding, local to allow local (from the perspective of ssh) forwarding only or remote to allow remote forwarding only. Note that disabling TCP forwarding does not improve security unless users are also denied shell access, as they can always install their own forwarders.
     * @summary Specified wether TCP forwarding is permitted
     * @param {String} allow - 'yes' == 'all': Allow TCP forwarding, 'no': Prevent all TCP forwarding, 'local': Allow local forwarding only, 'remote': Allow remote forwarding only.Default: 'yes'
     * @param {Function} callback
     */
    AllowTcpForwarding: function (allow, callback) {
        if (allow === "yes" || allow === "all" || allow === "no" || allow === "local" || allow === "remote") {
            require('./configuration').GetCurrent((config) => {
                if (config !== false) {
                    config.allowtcpforwarding = allow;
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            });
        } else {
            callback(false);
        }
    },
    /**
     * This keyword can be followed by a list of user name patterns, separated by spaces. If specified, login is allowed only for user names that match one of the patterns. Only user names are valid; a numerical user ID is not recognized. By default, login is allowed for all users. If the pattern takes the form USER@HOST then USER and HOST are separately checked, restricting logins to particular users from particular hosts. HOST criteria may additionally contain addresses to match in CIDR address/masklen format. The allow/deny directives are processed in the following order: DenyUsers, AllowUsers, DenyGroups, and finally AllowGroups.
     * @summary Allow the specified users to login.
     * @param {String[]} users
     * @param {Function} callback
     */
    AllowUsers: function (usernames, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                users.Users.Exists(usernames, (e) => {
                    var exists = true;
                    e.forEach(user => {
                        if (e[user] === false) {
                            exists = false;
                        }
                    });
                    if (exists === true) {
                        // @ts-ignore
                        usernames.push({
                            separator: " "
                        });
                        config.allowusers = usernames;
                        require('./configuration').Set(config, (written) => {
                            callback(written);
                        });
                    } else {
                        callback(false);
                    }
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * Specifies the authentication methods that must be successfully completed for a user to be granted access. This option must be followed by one or more comma-separated lists of authentication method names, or by the single string any to indicate the default behaviour of accepting any single authentication method. If the default is overridden, then successful authentication requires completion of every method in at least one of these lists.
     * For example, “publickey,password publickey,keyboard-interactive” would require the user to complete public key authentication, followed by either password or keyboard interactive authentication. Only methods that are next in one or more lists are offered at each stage, so for this example it would not be possible to attempt password or keyboard-interactive authentication before public key.
     * For keyboard interactive authentication it is also possible to restrict authentication to a specific device by appending a colon followed by the device identifier bsdauth, pam, or skey, depending on the server configuration. For example, “keyboard-interactive:bsdauth” would restrict keyboard interactive authentication to the bsdauth device.
     * If the publickey method is listed more than once, sshd verifies that keys that have been used successfully are not reused for subsequent authentications. For example, “publickey,publickey” requires successful authentication using two different public keys.
     * Note that each authentication method listed should also be explicitly enabled in the configuration.
     * The available authentication methods are: “gssapi-with-mic”, “hostbased”, “keyboard-interactive”, “none” (used for access to password-less accounts when PermitEmptyPasswords is enabled), “password” and “publickey”.
     * @summary Specifies the authentication methods that must be successfully completed for a user to be granted access.
     * @param {String[]} methods 
     * @param {Function} callback 
     */
    AuthenticationMethods: function (methods, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                // @ts-ignore
                methods.push({
                    separator: ","
                });
                config.authenticationmethods = methods;
                require('./configuration').Set(config, (written) => {
                    callback(written);
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * Specifies a program to be used to look up the user's public keys. The program must be owned by root, not writable by group or others and specified by an absolute path. Arguments to AuthorizedKeysCommand accept the tokens described in the TOKENS section. If no arguments are specified then the username of the target user is used.
     * The program should produce on standard output zero or more lines of authorized_keys output (see AUTHORIZED_KEYS in sshd(8)). If a key supplied by AuthorizedKeysCommand does not successfully authenticate and authorize the user then public key authentication continues using the usual AuthorizedKeysFile files. By default, no AuthorizedKeysCommand is run.
     * @summary Specifies a program to be used to look up the user's public keys. 
     * @param {String} path - Absolute path to the program.
     * @param {Function} callback
     */
    AuthorizedKeysCommand: function (path, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                if (files.IsFile(path) === true) {
                    config.authorizedkeyscommand = path;
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * Specifies the user under whose account the AuthorizedKeysCommand is run. It is recommended to use a dedicated user that has no other role on the host than running authorized keys commands. If AuthorizedKeysCommand is specified but AuthorizedKeysCommandUser is not, then sshd(8) will refuse to start.
     * @summary Specifies the user under whose account the AuthorizedKeysCommand is run.
     * @param {String} username
     * @param {Function} callback
     */
    AuthorizedKeysCommandUser: function (username, callback) {
        if (filters.Validate.Username(username) === true) {
            require('./configuration').GetCurrent((config) => {
                if (config !== false) {
                    users.Users.Exists(username, (exists) => {
                        if (exists === true) {
                            config.authorizedkeyscommanduser = username;
                            require('./configuration').Set(config, (written) => {
                                callback(written);
                            });
                        } else {
                            callback(false);
                        }
                    });
                } else {
                    callback(false);
                }
            });
        } else {
            callback(false);
        }
    },
    /**
     * Specifies the file that contains the public keys used for user authentication. The format is described in the AUTHORIZED_KEYS FILE FORMAT section of sshd. Arguments to AuthorizedKeysFile accept the tokens described in the TOKENS section. After expansion, AuthorizedKeysFile is taken to be an absolute path or one relative to the user's home directory. Multiple files may be listed, separated by whitespace. Alternately this option may be set to none to skip checking for user keys in files. The default is “.ssh/authorized_keys .ssh/authorized_keys2”.
     * @summary Specifies the file that contains the public keys used for user authentication.
     * @param {String[]} path
     * @param {Function} callback
     */
    AuthorizedKeysFile: function (path, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                var exists = true;
                path.forEach(element => {
                    if (files.IsFile(element) === false) {
                        exists = false;
                    }
                });
                if (exists === true) {
                    // @ts-ignore
                    path.push({
                        separator: ","
                    });
                    config.authorizedkeysfile = path;
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * Specifies a program to be used to generate the list of allowed certificate principals as per AuthorizedPrincipalsFile. The program must be owned by root, not writable by group or others and specified by an absolute path. Arguments to AuthorizedPrincipalsCommand accept the tokens described in the TOKENS section. If no arguments are specified then the username of the target user is used.
The program should produce on standard output zero or more lines of AuthorizedPrincipalsFile output. If either AuthorizedPrincipalsCommand or AuthorizedPrincipalsFile is specified, then certificates offered by the client for authentication must contain a principal that is listed. By default, no AuthorizedPrincipalsCommand is run.
     * @summary Specifies a program to be used to generate the list of allowed certificate principals as per AuthorizedPrincipalsFile.
     * @param {String} path - Absolute path to the program
     * @param {Function} callback
     */
    AuthorizedPrincipalsCommand: function (path, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                if (files.IsFile(path) === true) {
                    config.authorizedprincipalscommand = path;
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * Specifies the user under whose account the AuthorizedPrincipalsCommand is run. It is recommended to use a dedicated user that has no other role on the host than running authorized principals commands. If AuthorizedPrincipalsCommand is specified but AuthorizedPrincipalsCommandUser is not, then sshd will refuse to start.
     * @summary Specifies the user under whose account the AuthorizedPrincipalsCommand is run.
     * @param {String} username
     * @param {Function} callback
     */
    AuthorizedPrincipalsCommandUser: function (username, callback) {
        if (filters.Validate.Username(username) === true) {
            require('./configuration').GetCurrent((config) => {
                if (config !== false) {
                    users.Users.Exists(username, (exists) => {
                        if (exists === true) {
                            config.authorizedprincipalscommanduser = username;
                            require('./configuration').Set(config, (written) => {
                                callback(written);
                            });
                        } else {
                            callback(false);
                        }
                    });
                } else {
                    callback(false);
                }
            });
        } else {
            callback(false);
        }
    },
    /**
     * Specifies a file that lists principal names that are accepted for certificate authentication. When using certificates signed by a key listed in TrustedUserCAKeys, this file lists names, one of which must appear in the certificate for it to be accepted for authentication. Names are listed one per line preceded by key options (as described in AUTHORIZED_KEYS FILE FORMAT in sshd). Empty lines and comments starting with ‘#’ are ignored.
     * Arguments to AuthorizedPrincipalsFile accept the tokens described in the TOKENS section. After expansion, AuthorizedPrincipalsFile is taken to be an absolute path or one relative to the user's home directory. The default is none, i.e. not to use a principals file – in this case, the username of the user must appear in a certificate's principals list for it to be accepted.
     * Note that AuthorizedPrincipalsFile is only used when authentication proceeds using a CA listed in TrustedUserCAKeys and is not consulted for certification authorities trusted via ~/.ssh/authorized_keys, though the principals= key option offers a similar facility (see sshd for details).
     * @summary Specifies a file that lists principal names that are accepted for certificate authentication.
     * @param {String} path - Absolute path to the file
     * @param {Function} callback
     */
    AuthorizedPrincipalsFile: function (path, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                if (files.IsFile(path) === true) {
                    config.authorizedprincipalsfile = path;
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * The contents of the specified file are sent to the remote user before authentication is allowed. If the argument is none then no banner is displayed. By default, no banner is displayed.
     * @param {String} path - Absolute path to the file
     * @param {Function} callback
     */
    Banner: function (path, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                if (files.IsFile(path) === true) {
                    config.banner = path;
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * Specifies whether challenge-response authentication is allowed. All authentication styles from login.conf are supported. The default is yes.
     * @param {Boolean} allow
     * @param {Function} callback
     */
    ChallengeResponseAuthentication: function (allow, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                if (typeof allow === "boolean") {
                    if (allow === true) {
                        config.challengeresponseauthentication = "yes";
                    } else if (allow === false) {
                        config.challengeresponseauthentication = "no";
                    }
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * Specifies the pathname of a directory to chroot to after authentication. At session startup sshd checks that all components of the pathname are root-owned directories which are not writable by any other user or group. After the chroot, sshd changes the working directory to the user's home directory. Arguments to ChrootDirectory accept the tokens described in the TOKENS section.
     * The ChrootDirectory must contain the necessary files and directories to support the user's session. For an interactive session this requires at least a shell, typically sh, and basic /dev nodes such as null, zero, stdin, stdout, stderr, and tty devices. For file transfer sessions using SFTP no additional configuration of the environment is necessary if the in-process sftp-server is used, though sessions which use logging may require /dev/log inside the chroot directory on some operating systems (see sftp-server for details).
     * For safety, it is very important that the directory hierarchy be prevented from modification by other processes on the system (especially those outside the jail). Misconfiguration can lead to unsafe environments which sshd cannot detect.
     * The default is none, indicating not to chroot.
     * @summary Specifies the pathname of a directory to chroot to after authentication.
     * @param {String} path
     * @param {Function} callback
     */
    ChrootDirectory: function (path, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                if (files.IsDirectory(path) === true) {
                    config.chrootdirectory = path;
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * Specifies the ciphers allowed. Multiple ciphers must be comma-separated. If the specified value begins with a ‘+’ character, then the specified ciphers will be appended to the default set instead of replacing them. If the specified value begins with a ‘-’ character, then the specified ciphers (including wildcards) will be removed from the default set instead of replacing them.
     * The supported ciphers are: 3des-cbc, aes128-cbc, aes192-cbc, aes256-cbc, aes128-ctr, aes192-ctr, aes256-ctr, aes128-gcm@openssh.com, aes256-gcm@openssh.com, chacha20-poly1305@openssh.com
     * The default is: chacha20-poly1305@openssh.com, aes128-ctr,aes192-ctr,aes256-ctr, aes128-gcm@openssh.com,aes256-gcm@openssh.com
     * The list of available ciphers may also be obtained using “ssh -Q cipher”.
     * @summary Specifies the ciphers allowed.
     * @param {String[]} ciphers
     * @param {Function} callback
     */
    Ciphers: function (ciphers, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                // @ts-ignore
                ciphers.push({
                    separator: ","
                });
                config.ciphers = ciphers;
                require('./configuration').Set(config, (written) => {
                    callback(written);
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * Sets the number of client alive messages which may be sent without sshd receiving any messages back from the client. If this threshold is reached while client alive messages are being sent, sshd will disconnect the client, terminating the session. It is important to note that the use of client alive messages is very different from TCPKeepAlive. The client alive messages are sent through the encrypted channel and therefore will not be spoofable. The TCP keepalive option enabled by TCPKeepAlive is spoofable. The client alive mechanism is valuable when the client or server depend on knowing when a connection has become inactive.
     * The default value is 3. If ClientAliveInterval is set to 15, and ClientAliveCountMax is left at the default, unresponsive SSH clients will be disconnected after approximately 45 seconds.
     * @summary Sets the number of client alive messages which may be sent without sshd receiving any messages back from the client
     * @param {Number} number
     * @param {Function} callback
     */
    ClientAliveCountMax: function (number, callback) {
        number = parseInt(String(filters.KeepOnly.Numbers(String(number))));
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                config.clientalivecountmax = String(number);
                require('./configuration').Set(config, (written) => {
                    callback(written);
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * Sets a timeout interval in seconds after which if no data has been received from the client, sshd will send a message through the encrypted channel to request a response from the client. The default is 0, indicating that these messages will not be sent to the client.
     * @param {Number} number
     * @param {Function} callback
     */
    ClientAliveInterval: function (number, callback) {
        number = parseInt(String(filters.KeepOnly.Numbers(String(number))));
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                config.clientaliveinterval = String(number);
                require('./configuration').Set(config, (written) => {
                    callback(written);
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * Specifies whether compression is enabled after the user has authenticated successfully.
     * @param {Boolean} enable
     * @param {Function} callback
     */
    Compression: function (enable, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                if (typeof enable === "boolean") {
                    if (enable === true) {
                        config.compression = "yes";
                    } else if (enable === false) {
                        config.compression = "no";
                    }
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * This keyword can be followed by a list of group name patterns, separated by spaces. Login is disallowed for users whose primary group or supplementary group list matches one of the patterns. Only group names are valid; a numerical group ID is not recognized. By default, login is allowed for all groups. The allow/deny directives are processed in the following order: DenyUsers, AllowUsers, DenyGroups, and finally AllowGroups.
     * See PATTERNS in ssh_config for more information on patterns.
     * @summary This keyword can be followed by a list of group name patterns, separated by spaces.
     * @param {String[]} groups
     * @param {Function} callback
     */
    DenyGroups: function (groups, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                users.Groups.Exists(groups, (e) => {
                    var exists = true;
                    e.forEach(group => {
                        if (e[group] === false) {
                            exists = false;
                        }
                    });
                    if (exists === true) {
                        // @ts-ignore
                        groups.push({
                            separator: " "
                        });
                        config.denygroups = groups;
                        require('./configuration').Set(config, (written) => {
                            callback(written);
                        });
                    } else {
                        callback(false);
                    }
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * This keyword can be followed by a list of user name patterns, separated by spaces. Login is disallowed for user names that match one of the patterns. Only user names are valid; a numerical user ID is not recognized. By default, login is allowed for all users. If the pattern takes the form USER@HOST then USER and HOST are separately checked, restricting logins to particular users from particular hosts. HOST criteria may additionally contain addresses to match in CIDR address/masklen format. The allow/deny directives are processed in the following order: DenyUsers, AllowUsers, DenyGroups, and finally AllowGroups.
     * See PATTERNS in ssh_config for more information on patterns.
     * @summary This keyword can be followed by a list of user name patterns, separated by spaces.
     * @param {String[]} usernames
     * @param {Function} callback
     */
    DenyUsers: function (usernames, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                users.Users.Exists(usernames, (e) => {
                    var exists = true;
                    e.forEach(user => {
                        if (e[user] === false) {
                            exists = false;
                        }
                    });
                    if (exists === true) {
                        // @ts-ignore
                        usernames.push({
                            separator: " "
                        });
                        config.allowusers = usernames;
                        require('./configuration').Set(config, (written) => {
                            callback(written);
                        });
                    } else {
                        callback(false);
                    }
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * Disables all forwarding features, including X11, ssh-agent, TCP and StreamLocal. This option overrides all other forwarding-related options and may simplify restricted configurations.
     * @param {Boolean} disable
     * @param {Function} callback
     */
    DisableForwarding: function (disable, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                if (typeof disable === "boolean") {
                    if (disable === true) {
                        config.disableforwarding = "yes";
                    } else if (disable === false) {
                        config.disableforwarding = "no";
                    }
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * Writes a temporary file containing a list of authentication methods and public credentials (e.g. keys) used to authenticate the user. The location of the file is exposed to the user session through the SSH_USER_AUTH environment variable. The default is no.
     * @param {Boolean} enable
     * @param {Function} callback
     */
    ExposeAuthInfo: function (enable, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                if (typeof enable === "boolean") {
                    if (enable === true) {
                        config.exposeauthinfo = "yes";
                    } else if (enable === false) {
                        config.exposeauthinfo = "no";
                    }
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * Specifies the hash algorithm used when logging key fingerprints. Valid options are: md5 and sha256. The default is sha256.
     * @param {String} hash - Available: "md5" or "sha256"
     * @param {Function} callback
     */
    FingerprintHash: function (hash, callback) {
        if (hash === "md5" || hash === "sha256") {
            require('./configuration').GetCurrent((config) => {
                if (config !== false) {
                    config.fingerprinthash = hash;
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            });
        } else {
            callback(false);
        }
    },
    /**
     * Forces the execution of the command specified by ForceCommand, ignoring any command supplied by the client and ~/.ssh/rc if present. The command is invoked by using the user's login shell with the -c option. This applies to shell, command, or subsystem execution. It is most useful inside a Match block. The command originally supplied by the client is available in the SSH_ORIGINAL_COMMAND environment variable. Specifying a command of internal-sftp will force the use of an in-process SFTP server that requires no support files when used with ChrootDirectory. The default is none.
     * @summary Forces the execution of the command specified by ForceCommand, ignoring any command supplied by the client and ~/.ssh/rc if present.
     * @param {String} command
     * @param {Function} callback
     */
    ForceCommand: function (command, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                config.forcecommand = command;
                require('./configuration').Set(config, (written) => {
                    callback(written);
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * Specifies whether remote hosts are allowed to connect to ports forwarded for the client. By default, sshd binds remote port forwardings to the loopback address. This prevents other remote hosts from connecting to forwarded ports. GatewayPorts can be used to specify that sshd should allow remote port forwardings to bind to non-loopback addresses, thus allowing other hosts to connect. The argument may be no to force remote port forwardings to be available to the local host only, yes to force remote port forwardings to bind to the wildcard address, or clientspecified to allow the client to select the address to which the forwarding is bound. The default is no.
     * @summary Specifies whether remote hosts are allowed to connect to ports forwarded for the client.
     * @param {String} rule - "yes", "no", "clientspecified".
     * @param {Function} callback
     */
    GatewayPorts: function (rule, callback) {
        if (rule === "yes" || rule === "no" || rule === "clientspecified") {
            require('./configuration').GetCurrent((config) => {
                if (config !== false) {
                    config.gatewayports = rule;
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            });
        } else {
            callback(false);
        }
    },
    /**
     * Specifies whether user authentication based on GSSAPI is allowed. The default is no.
     * @param {Boolean} allow
     * @param {Function} callback
     */
    GSSAPIAuthentication: function (allow, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                if (typeof allow === "boolean") {
                    if (allow === true) {
                        config.gssapiauthentication = "yes";
                    } else if (allow === false) {
                        config.gssapiauthentication = "no";
                    }
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * Specifies whether to automatically destroy the user's credentials cache on logout. The default is yes.
     * @param {Boolean} cleanup
     * @param {Function} callback
     */
    GSSAPICleanupCredentials: function (cleanup, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                if (typeof cleanup === "boolean") {
                    if (cleanup === true) {
                        config.gssapicleanupcredentials = "yes";
                    } else if (cleanup === false) {
                        config.gssapicleanupcredentials = "no";
                    }
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * Determines whether to be strict about the identity of the GSSAPI acceptor a client authenticates against. If set to yes then the client must authenticate against the host service on the current hostname. If set to no then the client may authenticate against any service key stored in the machine's default store. This facility is provided to assist with operation on multi homed machines. The default is yes.
     * @summary Determines whether to be strict about the identity of the GSSAPI acceptor a client authenticates against.
     * @param {Boolean} enable
     * @param {Function} callback
     */
    GSSAPIStrictAcceptorCheck: function (enable, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                if (typeof enable === "boolean") {
                    if (enable === true) {
                        config.gssapistrictacceptorcheck = "yes";
                    } else if (enable === false) {
                        config.gssapistrictacceptorcheck = "no";
                    }
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * Specifies the key types that will be accepted for hostbased authentication as a comma-separated pattern list. Alternately if the specified value begins with a ‘+’ character, then the specified key types will be appended to the default set instead of replacing them. If the specified value begins with a ‘-’ character, then the specified key types (including wildcards) will be removed from the default set instead of replacing them. The default for this option is:
     * ecdsa-sha2-nistp256-cert-v01@openssh.com, ecdsa-sha2-nistp384-cert-v01@openssh.com, ecdsa-sha2-nistp521-cert-v01@openssh.com, ssh-ed25519-cert-v01@openssh.com, ssh-rsa-cert-v01@openssh.com, ecdsa-sha2-nistp256,ecdsa-sha2-nistp384,ecdsa-sha2-nistp521, ssh-ed25519,ssh-rsa
     * The list of available key types may also be obtained using “ssh -Q key”.
     * @summary Specifies the key types that will be accepted for hostbased authentication as a comma-separated pattern list.
     * @param {String[]} types
     * @param {Function} callback
     */
    HostbasedAcceptedKeyTypes: function (types, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                // @ts-ignore
                ciphers.push({
                    separator: ","
                });
                config.hostbasedacceptedkeytypes = types;
                require('./configuration').Set(config, (written) => {
                    callback(written);
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * Specifies whether rhosts or /etc/hosts.equiv authentication together with successful public key client host authentication is allowed (host-based authentication). The default is no.
     * @param {Boolean} allow 
     * @param {Function} callback
     */
    HostbasedAuthentication: function (allow, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                if (typeof allow === "boolean") {
                    if (allow === true) {
                        config.hostbasedauthentication = "yes";
                    } else if (allow === false) {
                        config.hostbasedauthentication = "no";
                    }
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * Specifies whether or not the server will attempt to perform a reverse name lookup when matching the name in the ~/.shosts, ~/.rhosts, and /etc/hosts.equiv files during HostbasedAuthentication. A setting of yes means that sshd uses the name supplied by the client rather than attempting to resolve the name from the TCP connection itself. The default is no.
     * @param {Boolean} enable
     * @param {Function} callback
     */
    HostbasedUsesNameFromPacketOnly: function (enable, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                if (typeof enable === "boolean") {
                    if (enable === true) {
                        config.hostbasedusesnamefrompacketonly = "yes";
                    } else if (enable === false) {
                        config.hostbasedusesnamefrompacketonly = "no";
                    }
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * Specifies a file containing a public host certificate. The certificate's public key must match a private host key already specified by HostKey. The default behaviour of sshd is not to load any certificates.
     * @param {String} path
     * @param {Function} callback
     */
    HostCertificate: function (path, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                if (files.IsFile(path) === true) {
                    config.hostcertificate = path;
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * Specifies a file containing a private host key used by SSH. The defaults are /etc/ssh/ssh_host_ecdsa_key, /etc/ssh/ssh_host_ed25519_key and /etc/ssh/ssh_host_rsa_key.
     * Note that sshd will refuse to use a file if it is group/world-accessible and that the HostKeyAlgorithms option restricts which of the keys are actually used by sshd.
     * It is possible to have multiple host key files. It is also possible to specify public host key files instead. In this case operations on the private key will be delegated to an ssh-agent.
     * @summary Specifies a file containing a private host key used by SSH.
     * @param {String[]} path
     * @param {Function} callback
     */
    HostKey: function (path, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                var exists = true;
                path.forEach(element => {
                    if (files.IsFile(element) === false) {
                        exists = false;
                    }
                });
                if (exists === true) {
                    // @ts-ignore
                    path.push({
                        separator: ","
                    });
                    config.hostkey = path;
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * Specifies the host key algorithms that the server offers. The list of available key types may also be obtained using “ssh -Q key”.
     * @param {String[]} algorithms
     * @param {Function} callback
     */
    HostKeyAlgorithms: function (algorithms, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                // @ts-ignore
                algorithms.push({
                    separator: ","
                });
                config.hostkeyalgorithms = algorithms;
                require('./configuration').Set(config, (written) => {
                    callback(written);
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * Specifies that .rhosts and .shosts files will not be used in HostbasedAuthentication.
     * /etc/hosts.equiv and /etc/shosts.equiv are still used. The default is yes.
     * @param {Boolean} ignore
     * @param {Function} callback
     */
    IgnoreRhosts: function (ignore, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                if (typeof ignore === "boolean") {
                    if (ignore === true) {
                        config.ignorerhosts = "yes";
                    } else if (ignore === false) {
                        config.ignorerhosts = "no";
                    }
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * Specifies whether sshd should ignore the user's ~/.ssh/known_hosts during HostbasedAuthentication and use only the system-wide known hosts file /etc/ssh/known_hosts. The default is no.
     * @param {Boolean} ignore
     * @param {Function} callback
     */
    IgnoreUserKnownHosts: function (ignore, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                if (typeof ignore === "boolean") {
                    if (ignore === true) {
                        config.ignorerhosts = "yes";
                    } else if (ignore === false) {
                        config.ignorerhosts = "no";
                    }
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * Specifies the IPv4 type-of-service or DSCP class for the connection. Accepted values are af11, af12, af13, af21, af22, af23, af31, af32, af33, af41, af42, af43, cs0, cs1, cs2, cs3, cs4, cs5, cs6, cs7, ef, lowdelay, throughput, reliability, a numeric value, or none to use the operating system default. This option may take one or two arguments, separated by whitespace. If one argument is specified, it is used as the packet class unconditionally. If two values are specified, the first is automatically selected for interactive sessions and the second for non-interactive sessions. The default is af21 (Low-Latency Data) for interactive sessions and cs1 (Lower Effort) for non-interactive sessions.
     * @summary Specifies the IPv4 type-of-service or DSCP class for the connection.
     * @param {String|String[]} typeOfService
     * @param {Function} callback
     */
    IPQoS: function (typeOfService, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                new Promise((resolve, reject) => {
                    if (Array.isArray(typeOfService)) {
                        if (typeOfService.length === 2) {
                            var exists;
                            exists = true;
                            typeOfService.forEach(element => {
                                if (element.match(/^(af11|af12|af13|af21|af22|af23|af31|af32|af33|af41|af42|af43|cs0|cs1|cs2|cs3|cs4|cs5|cs6|cs7|ef|lowdelay|throughput|reliability)$/) === null) {
                                    exists = false;
                                }
                            });
                            if (exists === true) {
                                // @ts-ignore
                                typeOfService.push({
                                    separator: " "
                                });
                                resolve(true);
                            } else {
                                resolve(false);
                            }
                        } else {
                            resolve(false);
                        }
                    } else {
                        resolve(typeOfService.match(/^(af11|af12|af13|af21|af22|af23|af31|af32|af33|af41|af42|af43|cs0|cs1|cs2|cs3|cs4|cs5|cs6|cs7|ef|lowdelay|throughput|reliability)$/) !== null);
                    }
                }).then((result) => {
                    if (result === true) {
                        config.ipqos = typeOfService;
                        require('./configuration').Set(config, (written) => {
                            callback(written);
                        });
                    } else {
                        callback(false);
                    }
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * Specifies whether to allow keyboard-interactive authentication. The argument to this keyword must be yes or no. The default is to use whatever value ChallengeResponseAuthentication is set to (by default yes).
     * @param {Boolean} allow
     * @param {Function} callback
     */
    KbdInteractiveAuthentication: function (allow, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                if (typeof allow === "boolean") {
                    if (allow === true) {
                        config.kdbinteractiveauthentication = "yes";
                    } else if (allow === false) {
                        config.kdbinteractiveauthentication = "no";
                    }
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * Specifies whether the password provided by the user for PasswordAuthentication will be validated through the Kerberos KDC. To use this option, the server needs a Kerberos servtab which allows the verification of the KDC's identity. The default is no.
     * @param {Boolean} allow
     * @param {Function} callback
     */
    KerberosAuthentication: function (allow, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                if (typeof allow === "boolean") {
                    if (allow === true) {
                        config.kerberosauthentication = "yes";
                    } else if (allow === false) {
                        config.kerberosauthentication = "no";
                    }
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * If AFS is active and the user has a Kerberos 5 TGT, attempt to acquire an AFS token before accessing the user's home directory. The default is no.
     * @param {Boolean} active
     * @param {Function} callback
     */
    KerberosGetAFSToken: function (active, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                if (typeof active === "boolean") {
                    if (active === true) {
                        config.kerberosgetafstoken = "yes";
                    } else if (active === false) {
                        config.kerberosgetafstoken = "no";
                    }
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * If password authentication through Kerberos fails then the password will be validated via any additional local mechanism such as /etc/passwd. The default is yes.
     * @param {Boolean} enable
     * @param {Function} callback
     */
    KerberosOrLocalPasswd: function (enable, callback) {
        require('./configuration').GetCurrent(config => {
            if (config !== false) {
                if (typeof enable === 'boolean') {
                    if (enable === true) {
                        config.kerberosorlocalpasswd = 'yes';
                    } else if (enable === false) {
                        config.kerberosorlocalpasswd = 'no';
                    }
                    require('./configuration').Set(config, written => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * Specifies whether to automatically destroy the user's ticket cache file on logout.
     * @param {Boolean} enable - Default: true.
     * @param {Function} callback
     */
    KerberosTicketCleanup: function (enable, callback) {
        require('./configuration').GetCurrent(config => {
            if (config !== false) {
                if (typeof enable === 'boolean') {
                    if (enable === true) {
                        config.kerberosticketcleanup = 'yes';
                    } else if (enable === false) {
                        config.kerberosticketcleanup = 'no';
                    }
                    require('./configuration').Set(config, written => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * Specifies the available KEX (Key Exchange) algorithms. Multiple algorithms must be comma-separated. Alternately if the specified value begins with a ‘+’ character, then the specified methods will be appended to the default set instead of replacing them. If the specified value begins with a ‘-’ character, then the specified methods (including wildcards) will be removed from the default set instead of replacing them.
     * The list of available key exchange algorithms may also be obtained using “ssh -Q kex”.
     * @summary Specifies the available KEX (Key Exchange) algorithms. Multiple algorithms must be comma-separated.
     * @param {String[]} algorithms - Default: ["curve25519-sha256,curve25519-sha256@libssh.org", "ecdh-sha2-nistp256,ecdh-sha2-nistp384,ecdh-sha2-nistp521", "diffie-hellman-group-exchange-sha256", "diffie-hellman-group16-sha512,diffie-hellman-group18-sha512", "diffie-hellman-group14-sha256,diffie-hellman-group14-sha1"]
     * @param {Function} callback
     */
    KexAlgorithms: function (algorithms, callback) {
        require('./configuration').GetCurrent(config => {
            if (config !== false) {
                // @ts-ignore
                algorithms.push({
                    separator: ','
                });
                config.kexalgorithms = algorithms;
                require('./configuration').Set(config, written => {
                    callback(written);
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * Specifies the local addresses sshd should listen on. The following forms may be used:
     * ListenAddress hostname|address [rdomain domain]
     * ListenAddress hostname:port [rdomain domain]
     * ListenAddress IPv4_address:port [rdomain domain]
     * ListenAddress [hostname|address]:port [rdomain domain]
     * The optional rdomain qualifier requests sshd listen in an explicit routing domain. If port is not specified, sshd will listen on the address and all Port options specified. The default is to listen on all local addresses on the current default routing domain. Multiple ListenAddress options are permitted. For more information on routing domains, see rdomain.
     * @summary Specifies the local addresses sshd should listen on.
     * @param {String} address
     * @param {Function} callback
     */
    ListenAddress: function (address, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                config.hostcertificate = address;
                require('./configuration').Set(config, (written) => {
                    callback(written);
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * The server disconnects after this time if the user has not successfully logged in. If the value is 0, there is no time limit. The default is 120 seconds.
     * @param {String|Number} time
     * @param {Function} callback
     */
    LoginGraceTime: function (time, callback) {
        time = parseInt(String(time));
        if (isNaN(time) === false) {
            require('./configuration').GetCurrent((config) => {
                if (config !== false) {
                    if (time > 0) {
                        config.logingracetime = String(time);
                        require('./configuration').Set(config, (written) => {
                            callback(written);
                        });
                    } else {
                        callback(false);
                    }
                } else {
                    callback(false);
                }
            });
        } else {
            callback(false);
        }
    },
    /**
     * Gives the verbosity level that is used when logging messages from sshd. The possible values are: QUIET, FATAL, ERROR, INFO, VERBOSE, DEBUG, DEBUG1, DEBUG2, and DEBUG3. The default is INFO. DEBUG and DEBUG1 are equivalent. DEBUG2 and DEBUG3 each specify higher levels of debugging output. Logging with a DEBUG level violates the privacy of users and is not recommended.
     * @summary Gives the verbosity level that is used when logging messages from sshd.
     * @param {String} level
     * @param {Function} callback
     */
    LogLevel: function (level, callback) {
        if (level.match(/^(QUIET|FATAL|ERROR|INFO|VERBOSE|DEBUG|DEBUG1|DEBUG2|DEBUG3)$/i) !== null) {
            require('./configuration').GetCurrent((config) => {
                if (config !== false) {
                    config.loglevel = level;
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            });
        } else {
            callback(false);
        }
    },
    /**
     * Specifies the available MAC (message authentication code) algorithms. The MAC algorithm is used for data integrity protection. Multiple algorithms must be comma-separated. If the specified value begins with a ‘+’ character, then the specified algorithms will be appended to the default set instead of replacing them. If the specified value begins with a ‘-’ character, then the specified algorithms (including wildcards) will be removed from the default set instead of replacing them.
     * The algorithms that contain “-etm” calculate the MAC after encryption (encrypt-then-mac). These are considered safer and their use recommended.
     * The list of available MAC algorithms may also be obtained using “ssh -Q mac”.
     * @summary Specifies the available MAC (message authentication code) algorithms.
     * @param {String[]} algorithms - Default: ["umac-64-etm@openssh.com,umac-128-etm@openssh.com", "hmac-sha2-256-etm@openssh.com,hmac-sha2-512-etm@openssh.com", "hmac-sha1-etm@openssh.com", "umac-64@openssh.com,umac-128@openssh.com", "hmac-sha2-256,hmac-sha2-512,hmac-sha1"]
     * @param {Function} callback
     */
    MACs: function (algorithms, callback) {
        require('./configuration').GetCurrent(config => {
            if (config !== false) {
                // @ts-ignore
                algorithms.push({
                    separator: ','
                });
                config.macs = algorithms;
                require('./configuration').Set(config, written => {
                    callback(written);
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * Introduces a conditional block. If all of the criteria on the Match line are satisfied, the keywords on the following lines override those set in the global section of the config file, until either another Match line or the end of the file. If a keyword appears in multiple Match blocks that are satisfied, only the first instance of the keyword is applied.
     * The arguments to Match are one or more criteria-pattern pairs or the single token All which matches all criteria. The available criteria are User, Group, Host, LocalAddress, LocalPort, RDomain, and Address (with RDomain representing the rdomain on which the connection was received.)
     * The match patterns may consist of single entries or comma-separated lists and may use the wildcard and negation operators described in the PATTERNS section of ssh_config.
     * The patterns in an Address criteria may additionally contain addresses to match in CIDR address/masklen format, such as 192.0.2.0/24 or 2001:db8::/32. Note that the mask length provided must be consistent with the address - it is an error to specify a mask length that is too long for the address or one with bits set in this host portion of the address. For example, 192.0.2.0/33 and 192.0.2.0/8, respectively.
     * Only a subset of keywords may be used on the lines following a Match keyword. Available keywords are AcceptEnv, AllowAgentForwarding, AllowGroups, AllowStreamLocalForwarding, AllowTcpForwarding, AllowUsers, AuthenticationMethods, AuthorizedKeysCommand, AuthorizedKeysCommandUser, AuthorizedKeysFile, AuthorizedPrincipalsCommand, AuthorizedPrincipalsCommandUser, AuthorizedPrincipalsFile, Banner, ChrootDirectory, ClientAliveCountMax, ClientAliveInterval, DenyGroups, DenyUsers, ForceCommand, GatewayPorts, GSSAPIAuthentication, HostbasedAcceptedKeyTypes, HostbasedAuthentication, HostbasedUsesNameFromPacketOnly, IPQoS, KbdInteractiveAuthentication, KerberosAuthentication, LogLevel, MaxAuthTries, MaxSessions, PasswordAuthentication, PermitEmptyPasswords, PermitListen, PermitOpen, PermitRootLogin, PermitTTY, PermitTunnel, PermitUserRC, PubkeyAcceptedKeyTypes, PubkeyAuthentication, RekeyLimit, RevokedKeys, RDomain, SetEnv, StreamLocalBindMask, StreamLocalBindUnlink, TrustedUserCAKeys, X11DisplayOffset, X11Forwarding and X11UseLocalHost.
     * @param {String} match
     * @param {Function} callback
     */
    Match: function (match, callback) {
        console.warn("There's no verification in this function");
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                config.match = match;
                require('./configuration').Set(config, (written) => {
                    callback(written);
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * Specifies the maximum number of authentication attempts permitted per connection. Once the number of failures reaches half this value, additional failures are logged.
     * @param {String|Number} tries - Default: 6.
     * @param {Function} callback
     */
    MaxAuthTries: function (tries, callback) {
        tries = parseInt(String(tries));
        if (isNaN(tries) === false) {
            require('./configuration').GetCurrent((config) => {
                if (config !== false) {
                    if (tries > 0) {
                        config.maxauthtries = String(tries);
                        require('./configuration').Set(config, (written) => {
                            callback(written);
                        });
                    } else {
                        callback(false);
                    }
                } else {
                    callback(false);
                }
            });
        } else {
            callback(false);
        }
    },
    /**
     * Specifies the maximum number of open shell, login or subsystem (e.g. sftp) sessions permitted per network connection. Multiple sessions may be established by clients that support connection multiplexing. Setting MaxSessions to 1 will effectively disable session multiplexing, whereas setting it to 0 will prevent all shell, login and subsystem sessions while still permitting forwarding.
     * @summary Specifies the maximum number of open shell, login or subsystem (e.g. sftp) sessions permitted per network connection. 
     * @param {String|Number} sessions - Default: 10.
     * @param {Function} callback
     */
    MaxSessions: function (sessions, callback) {
        sessions = parseInt(String(sessions));
        if (isNaN(sessions) === false) {
            require('./configuration').GetCurrent((config) => {
                if (config !== false) {
                    if (sessions > 0) {
                        config.maxauthtries = String(sessions);
                        require('./configuration').Set(config, (written) => {
                            callback(written);
                        });
                    } else {
                        callback(false);
                    }
                } else {
                    callback(false);
                }
            });
        } else {
            callback(false);
        }
    },
    /**
     * Specifies the maximum number of concurrent unauthenticated connections to the SSH daemon. Additional connections will be dropped until authentication succeeds or the LoginGraceTime expires for a connection.
     * Alternatively, random early drop can be enabled by specifying the three colon separated values start:rate:full (e.g. "10:30:60"). sshd will refuse connection attempts with a probability of rate/100 (30%) if there are currently start (10) unauthenticated connections. The probability increases linearly and all connection attempts are refused if the number of unauthenticated connections reaches full (60).
     * @summary Specifies the maximum number of concurrent unauthenticated connections to the SSH daemon.
     * @param {String[]|Number[]} startups - Default: [10, 30, 100]
     * @param {Function} callback
     */
    MaxStartups: function (startups, callback) {
        if (startups.length === 3) {
            require('./configuration').GetCurrent((config) => {
                if (config !== false) {
                    var ok = true;
                    // @ts-ignore
                    startups.forEach(startup => {
                        startup = parseInt(String(startup));
                        if (isNaN(startup)) {
                            ok = false;
                        }
                    });
                    if (ok === true) {
                        // @ts-ignore
                        startups.push({
                            separator: ":"
                        });
                        require('./configuration').Set(config, (written) => {
                            callback(written);
                        });
                    } else {
                        callback(false);
                    }
                } else {
                    callback(false);
                }
            });
        } else {
            callback(false);
        }
    },
    /**
     * Specifies whether password authentication is allowed. The default is yes.
     * @param {Boolean} allow
     * @param {Function} callback
     */
    PasswordAuthentication: function (allow, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                if (typeof allow === "boolean") {
                    if (allow === true) {
                        config.passwordauthentication = "yes";
                    } else if (allow === false) {
                        config.passwordauthentication = "no";
                    }
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * When password authentication is allowed, it specifies whether the server allows login to accounts with empty password strings. The default is no.
     * @param {Boolean} allow
     * @param {Function} callback
     */
    PermitEmptyPasswords: function (allow, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                if (typeof allow === "boolean") {
                    if (allow === true) {
                        config.permitemptypasswords = "yes";
                    } else if (allow === false) {
                        config.permitemptypasswords = "no";
                    }
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * Specifies the addresses/ports on which a remote TCP port forwarding may listen. The listen specification must be one of the following forms:
     * PermitListen host:port
     * PermitListen IPv4_addr:port
     * PermitListen [IPv6_addr]:port
     * Multiple permissions may be specified by separating them with whitespace. An argument of any can be used to remove all restrictions and permit any listen requests. An argument of none can be used to prohibit all listen requests. The host name may contain wildcards as described in the PATTERNS section in ssh_config. The wildcard ‘*’ can also be used in place of a port number to allow all ports. By default all port forwarding listen requests are permitted. Note that the GatewayPorts option may further restrict which addresses may be listened on.
     * @summary Specifies the addresses/ports on which a remote TCP port forwarding may listen.
     * @param {String} listen
     * @param {Function} callback
     */
    PermitListen: function (listen, callback) {
        console.warn("There's no verification in this function");
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                config.permitlisten = listen;
                require('./configuration').Set(config, (written) => {
                    callback(written);
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * Specifies the destinations to which TCP port forwarding is permitted. The forwarding specification must be one of the following forms:
     * PermitOpen host:port
     * PermitOpen IPv4_addr:port
     * PermitOpen [IPv6_addr]:port
     * Multiple forwards may be specified by separating them with whitespace. An argument of any can be used to remove all restrictions and permit any forwarding requests. An argument of none can be used to prohibit all forwarding requests. The wildcard ‘*’ can be used for host or port to allow all hosts or ports, respectively. By default all port forwarding requests are permitted.
     * @summary Specified the destinations to which TCP port forwarding is permitted.
     * @param {String} open
     * @param {Function} callback
     */
    PermitOpen: function (open, callback) {
        console.warn("There's no verification in this function");
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                config.permitopen = open;
                require('./configuration').Set(config, (written) => {
                    callback(written);
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * Specifies whether root can log in using ssh. The argument must be yes, prohibit-password, forced-commands-only, or no. The default is prohibit-password.
     * If this option is set to prohibit-password (or its deprecated alias, without-password), password and keyboard-interactive authentication are disabled for root.
     * If this option is set to forced-commands-only, root login with public key authentication will be allowed, but only if the command option has been specified (which may be useful for taking remote backups even if root login is normally not allowed). All other authentication methods are disabled for root.
     * If this option is set to no, root is not allowed to log in.
     * @summary Specifies whether root can log in using ssh.
     * @param {String} permit - yes, prohibit-password, forced-commands-only, no. Default: prohibit-password. 
     * @param {Function} callback
     */
    PermitRootLogin: function (permit, callback) {
        if (permit.match(/^(yes|prohibit-password|forced-commands-only|no)$/) !== null) {
            require('./configuration').GetCurrent((config) => {
                if (config !== false) {
                    config.permitrootlogin = open;
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            });
        } else {
            callback(false);
        }
    },
    /**
     * Specifies whether pty allocation is permitted. The default is yes.
     * @param {Boolean} permit
     * @param {Function} callback
     */
    PermitTTY: function (permit, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                if (typeof permit === "boolean") {
                    if (permit === true) {
                        config.permittty = "yes";
                    } else if (permit === false) {
                        config.permittty = "no";
                    }
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * Specifies whether tun device forwarding is allowed. The argument must be yes, point-to-point (layer 3), ethernet (layer 2), or no. Specifying yes permits both point-to-point and ethernet. The default is no.
     * Independent of this setting, the permissions of the selected tun device must allow access to the user.
     * @summary Specifies whether tun device forwarding is allowed.
     */
    PermitTunnel: function (permit, callback) {
        if (permit.match(/^(yes|point-to-point|ethernet|no)$/) !== null) {
            require('./configuration').GetCurrent((config) => {
                if (config !== false) {
                    config.permittunnel = open;
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            });
        } else {
            callback(false);
        }
    },
    /**
     * Specifies whether ~/.ssh/environment and environment= options in ~/.ssh/authorized_keys are processed by sshd. The default is no. Enabling environment processing may enable users to bypass access restrictions in some configurations using mechanisms such as LD_PRELOAD.
     * @summary Specifies whether ~/.ssh/environment and environment= options in ~/.ssh/authorized_keys are processed by sshd.
     * @param {Boolean} permit
     * @param {Function} callback
     */
    PermitUserEnvironment: function (permit, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                if (typeof permit === "boolean") {
                    if (permit === true) {
                        config.permituserenvironment = "yes";
                    } else if (permit === false) {
                        config.permituserenvironment = "no";
                    }
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * Specifies whether any ~/.ssh/rc file is executed. The default is yes.
     * @param {String} permit
     * @param {Function} callback
     */
    PermitUserRC: function (permit, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                if (typeof permit === "boolean") {
                    if (permit === true) {
                        config.permituserrc = "yes";
                    } else if (permit === false) {
                        config.permituserrc = "no";
                    }
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * Specifies the file that contains the process ID of the SSH daemon, or none to not write one. The default is /var/run/sshd.pid.
     * @param {String} path - a path or "none"
     * @param {Function} callback
     */
    PidFile: function (path, callback) {
        if (path === "none") {
            require('./configuration').Default("pidfile", (reset) => {
                callback(reset);
            });
        } else {
            require('./configuration').GetCurrent((config) => {
                if (config !== false) {
                    if (files.IsFile(path) === true) {
                        config.pidfile = path;
                        require('./configuration').Set(config, (written) => {
                            callback(written);
                        });
                    } else {
                        callback(false);
                    }
                } else {
                    callback(false);
                }
            });
        }
    },
    /**
     * Specifies the port number that sshd listens on. Multiple options of this type are permitted.
     * @param {Number} port - Port number between 1 and 65536. Default: 22
     * @param {Function} callback
     */
    ChangePort: function (port, callback) {
        port = parseInt(String(port));
        if (isNaN(port) === false) {
            if (port > 0 && port < 65536) {
                require('./configuration').GetCurrent((config) => {
                    config.port = String(port);
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                });
            } else {
                callback(false);
            }
        } else {
            callback(false);
        }
    },
    /**
     * Specifies whether sshd should print the date and time of the last user login when a user logs in interactively. The default is yes.
     * @param {Boolean} enable
     * @param {Function} callback
     */
    PrintLastLog: function (enable, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                if (typeof enable === "boolean") {
                    if (enable === true) {
                        config.printlastlog = "yes";
                    } else if (enable === false) {
                        config.printlastlog = "no";
                    }
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * Specifies whether sshd should print /etc/motd when a user logs in interactively. (On some systems it is also printed by the shell, /etc/profile, or equivalent.) The default is yes.
     * @param {Boolean} enable
     * @param {Function} callback
     */
    PrintMotd: function (enable, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                if (typeof enable === "boolean") {
                    if (enable === true) {
                        config.printmotd = "yes";
                    } else if (enable === false) {
                        config.printmotd = "no";
                    }
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * Specifies the key types that will be accepted for public key authentication as a comma-separated pattern list. Alternately if the specified value begins with a ‘+’ character, then the specified key types will be appended to the default set instead of replacing them. If the specified value begins with a ‘-’ character, then the specified key types (including wildcards) will be removed from the default set instead of replacing them.
     * The list of available key types may also be obtained using “ssh -Q key”.
     * @summary Specifies the key types that will be accepted for public key authentication as a comma-separated pattern list.
     * @param {String[]} keys - Default: ["ecdsa-sha2-nistp256-cert-v01@openssh.com", "ecdsa-sha2-nistp384-cert-v01@openssh.com", "ecdsa-sha2-nistp521-cert-v01@openssh.com", "ssh-ed25519-cert-v01@openssh.com", "ssh-rsa-cert-v01@openssh.com", "ecdsa-sha2-nistp256,ecdsa-sha2-nistp384,ecdsa-sha2-nistp521", "ssh-ed25519,ssh-rsa"]
     * @param {Function} callback
     */
    PubkeyAcceptedKeyTypes: function (keys, callback) {
        if (Array.isArray(keys)) {
            require('./configuration').GetCurrent((config) => {
                if (config !== false) {
                    // @ts-ignore
                    keys.push({
                        separator: ","
                    });
                    config.authenticationmethods = keys;
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            });
        } else {
            callback(false);
        }
    },
    /**
     * Specifies whether public key authentication is allowed. The default is yes.
     * @param {Boolean} allow
     * @param {Function} callback
     */
    PubkeyAuthentication: function (allow, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                if (typeof allow === "boolean") {
                    if (allow === true) {
                        config.pubkeyauthentication = "yes";
                    } else if (allow === false) {
                        config.pubkeyauthentication = "no";
                    }
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * Specifies the maximum amount of data that may be transmitted before the session key is renegotiated, optionally followed a maximum amount of time that may pass before the session key is renegotiated. The first argument is specified in bytes and may have a suffix of ‘K’, ‘M’, or ‘G’ to indicate Kilobytes, Megabytes, or Gigabytes, respectively. The default is between ‘1G’ and ‘4G’, depending on the cipher. The optional second value is specified in seconds and may use any of the units documented in the TIME FORMATS section. The default value for RekeyLimit is default none, which means that rekeying is performed after the cipher's default amount of data has been sent or received and no time based rekeying is done.
     * @param {String} amount
     * @param {Function} callback
     */
    RekeyLimit: function (amount, callback) {
        console.warn("There's no verification in this function");
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                config.rekeylimit = amount;
                require('./configuration').Set(config, (written) => {
                    callback(false);
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * Specifies revoked public keys file, or none to not use one. Keys listed in this file will be refused for public key authentication. Note that if this file is not readable, then public key authentication will be refused for all users. Keys may be specified as a text file, listing one public key per line, or as an OpenSSH Key Revocation List (KRL) as generated by ssh-keygen. For more information on KRLs, see the KEY REVOCATION LISTS section in ssh-keygen.
     * @summary Specifies revoked public keys file, or none to not use one.
     * @param {String} path
     * @param {Function} callback
     */
    RevokedKeys: function (path, callback) {
        if (path === "none") {
            require('./configuration').Default("revokedkeys", (reset) => {
                callback(reset);
            });
        } else {
            require('./configuration').GetCurrent((config) => {
                if (config !== false) {
                    if (files.IsFile(path) === true) {
                        config.revokedkeys = path;
                        require('./configuration').Set(config, (written) => {
                            callback(written);
                        });
                    } else {
                        callback(false);
                    }
                } else {
                    callback(false);
                }
            });
        }
    },
    /**
     * Specifies an explicit routing domain that is applied after authentication has completed. The user session, as well and any forwarded or listening IP sockets, will be bound to this rdomain. If the routing domain is set to %D, then the domain in which the incoming connection was received will be applied.
     * @summary Specifies an explicit routing domain that is applied after authentication has completed.
     * @param {String} routing
     * @param {Function} callback
     */
    RDomain: function (routing, callback) {
        console.warn("There's no verification in this function");
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                config.rdomain = routing;
                require('./configuration').Set(config, (written) => {
                    callback(false);
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * Specifies one or more environment variables to set in child sessions started by sshd as “NAME=VALUE”. The environment value may be quoted (e.g. if it contains whitespace characters). Environment variables set by SetEnv override the default environment and any variables specified by the user via AcceptEnv or PermitUserEnvironment.
     * @summary Specifies one or more environment variables to set in child sessions started by sshd as “NAME=VALUE”.
     * @param {String} routing
     * @param {Function} callback
     */
    SetEnv: function (env, callback) {
        console.warn("There's no verification in this function");
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                config.setenv = env;
                require('./configuration').Set(config, (written) => {
                    callback(false);
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * Sets the octal file creation mode mask (umask) used when creating a Unix-domain socket file for local or remote port forwarding. This option is only used for port forwarding to a Unix-domain socket file.
     * The default value is 0177, which creates a Unix-domain socket file that is readable and writable only by the owner. Note that not all operating systems honor the file mode on Unix-domain socket files.
     * @summary Sets the octal file creation mode mask (umask) used when creating a Unix-domain socket file for local or remote port forwarding.
     * @param {String} mask - Default 0177
     * @param {Function} callback
     */
    StreamLocalBindMask: function (mask, callback) {
        if (mask.length === 4) {
            if (isNaN(parseInt(mask)) === false) {
                var ok = true;
                for (var i = 0; i < mask.length; i++) {
                    if (parseInt(mask.charAt(i)) < 0 || parseInt(mask.charAt(i)) > 7) {
                        ok = false;
                        break;
                    }
                }
                if (ok === true) {
                    require('./configuration').GetCurrent((config) => {
                        if (config !== false) {
                            config.streamlocalbindmask = mask;
                            require('./configuration').Set(config, (written) => {
                                callback(false);
                            });
                        } else {
                            callback(false);
                        }
                    });
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        } else {
            callback(false);
        }
    },
    /**
     * Specifies whether to remove an existing Unix-domain socket file for local or remote port forwarding before creating a new one. If the socket file already exists and StreamLocalBindUnlink is not enabled, sshd will be unable to forward the port to the Unix-domain socket file. This option is only used for port forwarding to a Unix-domain socket file.
     * The argument must be yes or no. The default is no.
     * @summary Specifies whether to remove an existing Unix-domain socket file for local or remote port forwarding before creating a new one.
     * @param {Boolean} unlink
     * @param {Function} callback
     */
    StreamLocalBindUnlink: function (unlink, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                if (typeof unlink === "boolean") {
                    if (unlink === true) {
                        config.streamlocalbindunlink = "yes";
                    } else if (unlink === false) {
                        config.streamlocalbindunlink = "no";
                    }
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * Specifies whether sshd should check file modes and ownership of the user's files and home directory before accepting login. This is normally desirable because novices sometimes accidentally leave their directory or files world-writable. The default is yes. Note that this does not apply to ChrootDirectory, whose permissions and ownership are checked unconditionally.
     * @summary Specifies whether sshd should check file modes and ownership of the user's files and home directory before accepting login.
     * @param {Boolean} enable
     * @param {Function} callback
     */
    StrictModes: function (enable, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                if (typeof enable === "boolean") {
                    if (enable === true) {
                        config.strictmodes = "yes";
                    } else if (enable === false) {
                        config.strictmodes = "no";
                    }
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * Configures an external subsystem (e.g. file transfer daemon). Arguments should be a subsystem name and a command (with optional arguments) to execute upon subsystem request.
     * The command sftp-server implements the SFTP file transfer subsystem.
     * Alternately the name internal-sftp implements an in-process SFTP server. This may simplify configurations using ChrootDirectory to force a different filesystem root on clients.
     * By default no subsystems are defined.
     * @summary Configures an external subsystem (e.g. file transfer daemon).
     * @param {String} subsystemArgs
     * @param {Function} callback
     */
    Subsystem: function (subsystemArgs, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                config.subsystem = subsystemArgs;
                require('./configuration').Set(config, (written) => {
                    callback(written);
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * Gives the facility code that is used when logging messages from sshd.
     * @param {String} syslog - DAEMON, USER, AUTH, LOCAL0, LOCAL1, LOCAL2, LOCAL3, LOCAL4, LOCAL5, LOCAL6, LOCAL7. Default: AUTH
     * @param {Function} callback
     */
    SyslogFacility: function (syslog, callback) {
        if (syslog.match(/^(DAEMON|USER|AUTH|LOCAL0|LOCAL1|LOCAL2|LOCAL3|LOCAL4|LOCAL5|LOCAL6|LOCAL7)$/i) !== null) {
            require('./configuration').GetCurrent((config) => {
                if (config !== false) {
                    config.syslogfacility = syslog;
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            });
        } else {
            callback(false);
        }
    },
    /**
     * Specifies whether the system should send TCP keepalive messages to the other side. If they are sent, death of the connection or crash of one of the machines will be properly noticed. However, this means that connections will die if the route is down temporarily, and some people find it annoying. On the other hand, if TCP keepalives are not sent, sessions may hang indefinitely on the server, leaving “ghost” users and consuming server resources.
     * The default is yes (to send TCP keepalive messages), and the server will notice if the network goes down or the client host crashes. This avoids infinitely hanging sessions.
     * To disable TCP keepalive messages, the value should be set to no.
     * @summary Specifies whether the system should send TCP keepalive messages to the other side.
     * @param {Boolean} keepalive
     * @param {Function} callback
     */
    TCPKeepAlive: function (keepalive, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                if (typeof keepalive === "boolean") {
                    if (keepalive === true) {
                        config.tcpkeepalive = "yes";
                    } else if (keepalive === false) {
                        config.tcpkeepalive = "no";
                    }
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * Specifies a file containing public keys of certificate authorities that are trusted to sign user certificates for authentication, or none to not use one. Keys are listed one per line; empty lines and comments starting with ‘#’ are allowed. If a certificate is presented for authentication and has its signing CA key listed in this file, then it may be used for authentication for any user listed in the certificate's principals list. Note that certificates that lack a list of principals will not be permitted for authentication using TrustedUserCAKeys. For more details on certificates, see the CERTIFICATES section in ssh-keygen.
     * @summary Specifies a file containing public keys of certificate authorities that are trusted to sign user certificates for authentication, or none to not use one.
     * @param {String} path
     * @param {Function} callback
     */
    TrustedUserCAKeys: function (path, callback) {
        if (path === "none") {
            require('./configuration').Default("trustedusercakeys", (reset) => {
                callback(reset);
            });
        } else {
            require('./configuration').GetCurrent((config) => {
                if (config !== false) {
                    if (files.IsFile(path) === true) {
                        config.trustedusercakeys = path;
                        require('./configuration').Set(config, (written) => {
                            callback(written);
                        });
                    } else {
                        callback(false);
                    }
                } else {
                    callback(false);
                }
            });
        }
    },
    /**
     * Specifies whether sshd should look up the remote host name, and to check that the resolved host name for the remote IP address maps back to the very same IP address. 
     * If this option is set to no (the default) then only addresses and not host names may be used in ~/.ssh/authorized_keys from and sshd_config Match Host directives.
     * @summary Specifies whether sshd should look up the remote host name, and to check that the resolved host name for the remote IP address maps back to the very same IP address. 
     * @param {Boolean} enable
     * @param {Function} callback
     */
    UseDNS: function (enable, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                if (typeof enable === "boolean") {
                    if (enable === true) {
                        config.usedns = "yes";
                    } else if (enable === false) {
                        config.usedns = "no";
                    }
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * No documentation
     * @param {Boolean} enable
     * @param {Function} callback
     */
    UsePam: function (enable, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                if (typeof enable === "boolean") {
                    if (enable === true) {
                        config.usepam = "yes";
                    } else if (enable === false) {
                        config.usepam = "no";
                    }
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * Optionally specifies additional text to append to the SSH protocol banner sent by the server upon connection. The default is none.
     * @param {String} text - "none" for no value.
     * @param {Function} callback
     */
    VersionAddendum: function (text, callback) {
        if (text.match(/^(none)$/i)) {
            require('./configuration').Default("versionaddendum", (reset) => {
                callback(reset);
            });
        } else {
            require('./configuration').GetCurrent((config) => {
                config.versionaddendum = text;
                require('./configuration').Set(config, (written) => {
                    callback(written);
                });
            });
        }
    },
    /**
     * Specifies the first display number available for sshd's X11 forwarding. This prevents sshd from interfering with real X11 servers.
     * @param {String|Number} displayNumber = Default: 10
     * @param {Function} callback
     */
    X11DisplayOffset: function (displayNumber, callback) {
        displayNumber = parseInt(String(displayNumber));
        if (isNaN(displayNumber) === false) {
            require('./configuration').GetCurrent((config) => {
                if (config !== false) {
                    if (displayNumber > 0) {
                        config.x11displayoffset = String(displayNumber);
                        require('./configuration').Set(config, (written) => {
                            callback(written);
                        });
                    } else {
                        callback(false);
                    }
                } else {
                    callback(false);
                }
            });
        } else {
            callback(false);
        }
    },
    /**
     * Specifies whether X11 forwarding is permitted. The argument must be yes or no. The default is no.
     * When X11 forwarding is enabled, there may be additional exposure to the server and to client displays if the sshd proxy display is configured to listen on the wildcard address (see X11UseLocalhost), though this is not the default. Additionally, the authentication spoofing and authentication data verification and substitution occur on the client side. The security risk of using X11 forwarding is that the client's X11 display server may be exposed to attack when the SSH client requests forwarding (see the warnings for ForwardX11 in ssh_config). A system administrator may have a stance in which they want to protect clients that may expose themselves to attack by unwittingly requesting X11 forwarding, which can warrant a no setting.
     * Note that disabling X11 forwarding does not prevent users from forwarding X11 traffic, as users can always install their own forwarders.
     * @summary Specifies whether X11 forwarding is permitted. The argument must be yes or no. The default is no. 
     * @param {Boolean} enable
     * @param {Function} callback
     */
    X11Forwarding: function (enable, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                if (typeof enable === "boolean") {
                    if (enable === true) {
                        config.x11forwarding = "yes";
                    } else if (enable === false) {
                        config.x11forwarding = "no";
                    }
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * Specifies whether sshd should bind the X11 forwarding server to the loopback address or to the wildcard address. By default, sshd binds the forwarding server to the loopback address and sets the hostname part of the DISPLAY environment variable to localhost. This prevents remote hosts from connecting to the proxy display. However, some older X11 clients may not function with this configuration. X11UseLocalhost may be set to no to specify that the forwarding server should be bound to the wildcard address. The argument must be yes or no. The default is yes.
     * @summary Specifies whether sshd should bind the X11 forwarding server to the loopback address or to the wildcard address.
     * @param {Boolean} use
     * @param {Function} callback
     */
    X11UseLocalhost: function (use, callback) {
        require('./configuration').GetCurrent((config) => {
            if (config !== false) {
                if (typeof use === "boolean") {
                    if (use === true) {
                        config.x11forwarding = "yes";
                    } else if (use === false) {
                        config.x11forwarding = "no";
                    }
                    require('./configuration').Set(config, (written) => {
                        callback(written);
                    });
                } else {
                    callback(false);
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * Specifies the full pathname of the xauth program, or none to not use one. The default is /usr/X11R6/bin/xauth.
     * @param {String} path - path or "none"
     * @param {Function} callback
     */
    XAuthLocation: function (path, callback) {
        if (path === "none") {
            require('./configuration').Default("xauthlocation", (reset) => {
                callback(reset);
            });
        } else {
            require('./configuration').GetCurrent((config) => {
                if (config !== false) {
                    if (files.IsFile(path) === true) {
                        config.xauthlocation = path;
                        require('./configuration').Set(config, (written) => {
                            callback(written);
                        });
                    } else {
                        callback(false);
                    }
                } else {
                    callback(false);
                }
            });
        }
    }
};