var basis = require('alpi_basis');
var users = require('alpi_users');
var files = require('alpi_files');

module.exports = {

    ////////////////////// https://man.openbsd.org/sshd_config //////////////////////

    /**
     * @summary Check and change permissions of the sshd configuration file.
     * @param {Function} callback
     */
    SetupCheck: function (callback) {
        require('../ssh').IsServerInstalled((installed) => {
            if (installed === true) {
                if (files.IsFile("/etc/ssh/sshd_config") === true) {
                    files.FileInformations("/etc/ssh/sshd_config", (informations) => {
                        if (informations !== false) {
                            new Promise((resolve, reject) => {
                                //check if the group "admin_ssh" exists
                                users.Groups.Exists("admin_ssh", (exists) => {
                                    if (exists === false) {
                                        //create it if it isn't
                                        users.Groups.Create("admin_ssh", (e) => {
                                            resolve(e);
                                        });
                                    } else {
                                        resolve(true);
                                    }
                                });
                            }).then((result) => {
                                return new Promise((resolve, reject) => {
                                    if (result === true) {
                                        //get the current user
                                        basis.System.Whoami((user) => {
                                            if (user !== false) {
                                                //check if it belong to the group "admin_ssh"
                                                users.Groups.UserBelongToGroup(user, 'admin_ssh', (belongto) => {
                                                    if (belongto === true) {
                                                        resolve(true);
                                                    } else {
                                                        //add the secondary group "admin_ssh" to the current user
                                                        users.Groups.AddUserSecondaryGroupWithoutPassword(user, "admin_ssh", (added) => {
                                                            resolve(added);
                                                        });
                                                    }
                                                });
                                            } else {
                                                resolve(false);
                                            }
                                        });
                                    } else {
                                        resolve(false);
                                    }
                                });
                            }).then((result) => {
                                return new Promise((resolve, reject) => {
                                    if (result === true) {
                                        //check owner
                                        if (informations.owner + ":" + informations.group !== "root:admin_ssh") {
                                            basis.System.ShellCommand("sudo chown root:admin_ssh /etc/ssh/sshd_config", (e) => {
                                                resolve(e.stderr === "");
                                            });
                                        } else {
                                            resolve(true);
                                        }
                                    } else {
                                        resolve(false);
                                    }
                                });
                            }).then(result => {
                                if (result === true) {
                                    //check rights
                                    if (informations.rights !== "-rw-rw-r--") {
                                        basis.System.ShellCommand("sudo chmod 664 /etc/ssh/sshd_config", (e) => {
                                            callback(e.stderr === "");
                                        });
                                    } else {
                                        callback(true);
                                    }
                                } else {
                                    callback(false);
                                }
                            });
                        } else {
                            callback(false);
                        }
                    });
                } else {
                    basis.System.ShellCommand("sudo touch /etc/ssh/sshd_config", (e) => {
                        if (e.stderr === "") {
                            module.exports.SetupCheck((e) => {
                                callback(e);
                            });
                        } else {
                            callback(false);
                        }
                    });
                }
            } else {
                callback(false);
            }
        });
    },
    /**
     * @summary Return a json array containing default sshd configuration.
     * @param {Function} callback
     */
    GetDefault: function (callback) {
        module.exports.SetupCheck(check => {
            if (check === true) {
                module.exports.GetCurrent(currentConfig => {
                    if (currentConfig !== false) {
                        files.EmptyFile("/etc/ssh/sshd_config", empty => {
                            if (empty === true) {
                                module.exports.GetCurrent(defaultConfig => {
                                    module.exports.Set(currentConfig, e => {});
                                    callback(defaultConfig);
                                });
                            } else {
                                callback(false);
                            }
                        });
                    } else {
                        callback(false);
                    }
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * @summary Return a json array containing the current sshd configuration.
     * @param {Function} callback
     */
    GetCurrent: function (callback) {
        module.exports.SetupCheck((check) => {
            if (check === true) {
                basis.System.ShellCommand("sudo sshd -T | sort", (config) => {
                    if (config.error === null) {
                        config = config.stdout.trim().split("\n");
                        var result = {};
                        config.forEach(line => {
                            line = line.trim().split(" ");
                            var setting = line.shift().trim();
                            var value = line.join(" ").trim();
                            if (value.includes(',')) {
                                value = value.split(",");
                                value.push({
                                    separator: ","
                                });
                            } else if (value.includes(' ')) {
                                value = value.split(" ");
                                value.push({
                                    separator: " "
                                });
                            }
                            result[setting] = value;
                        });
                        callback(result);
                    } else {
                        callback(false);
                    }
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * @summary Set the configuration in parameter.
     * @param {Object} config configuration json array
     * @param {Function} callback
     */
    Set: function (config, callback) {
        module.exports.SetupCheck((permission) => {
            if (permission === true) {
                var result = "";
                Object.keys(config).forEach((key) => {
                    if (Array.isArray(config[key])) {
                        // @ts-ignore
                        config[key] = config[key].join(config[key].pop().separator);
                    }
                    result += key + " " + config[key] + "\n";
                });
                console.log(result);
                files.OverwriteFileContent("/etc/ssh/sshd_config", result, (e) => {
                    callback(e);
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * @summary Reset the configuration file with the default values.
     * @param {Function} callback
     */
    Reset: function (callback) {
        module.exports.GetDefault(defaultConfig => {
            if (defaultConfig !== false) {
                module.exports.Set(defaultConfig, e => {
                    callback(e);
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * @summary Contains multiple functions to change sshd configuration setting by setting.
     */
    SpecificAction: require('./specific')
};