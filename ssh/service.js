var services = require('alpi_services');

module.exports = {
    Status: function (callback) {
        services.Status("sshd", (status) => {
            callback(status);
        });
    },
    Start: function (callback) {
        services.Start("sshd", (started) => {
            callback(started);
        });
    },
    Stop: function (callback) {
        services.Stop("sshd", (stopped) => {
            callback(stopped);
        });
    },
    BootStatus: function (callback) {
        services.BootStatus("sshd", (bootStatus) => {
            callback(bootStatus);
        });
    },
    Enable: function (callback) {
        services.Enable("sshd", (enabled) => {
            callback(enabled);
        });
    },
    Disable: function (callback) {
        services.Disable("sshd", (disabled) => {
            callback(disabled);
        });
    }
};