var basis = require('alpi_basis');

module.exports = {
    /**
     * @summary Check if the server package is installed.
     * @param {Function} callback 
     */
    IsServerInstalled: function (callback) {
        basis.System.PackageManager((packageManager) => {
            if (packageManager === "pacman" || packageManager === "apt") {
                basis.System.isPackageInstalled("samba", (installed) => {
                    callback(installed);
                });
            } else {
                //it is assumed it is installed
                callback(true);
            }
        });
    },
    /**
     * @summary Install the server package.
     * @param {Function} callback 
     */
    InstallServer: function (callback) {
        basis.System.InstallPackage("samba", (installed) => {
            callback(installed === true);
        });
    },
    /**
     * @summary Show samba version.
     * @param {Function} callback 
     */
    Version: function (callback) {
        module.exports.sshServerIsInstalled((installed) => {
            if (installed === true) {
                basis.System.ShellCommand("samba -V", (version) => {
                    if (version.error === null) {
                        callback(version.stdout.trim());
                    } else {
                        callback(false);
                    }
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * @summary SMB service management.
     */
    Service: require('./smb/service'),
    /**
     * @summary SMB configuration.
     */
    Configuration: require('./smb/configuration'),
    Users: require('./smb/users')
};