var services = require('alpi_services');

module.exports = {
    Status: function (callback) {
        services.Status("vsftpd", (status) => {
            callback(status);
        });
    },
    Start: function (callback) {
        services.Start("vsftpd", (started) => {
            callback(started);
        });
    },
    Stop: function (callback) {
        services.Stop("vsftpd", (stopped) => {
            callback(stopped);
        });
    },
    BootStatus: function (callback) {
        services.BootStatus("vsftpd", (bootStatus) => {
            callback(bootStatus);
        });
    },
    Enable: function (callback) {
        services.Enable("vsftpd", (enabled) => {
            callback(enabled);
        });
    },
    Disable: function (callback) {
        services.Disable("vsftpd", (disabled) => {
            callback(disabled);
        });
    }
};