var basis = require('alpi_basis');

module.exports = {
    /**
     * @summary Check if the server package is installed.
     * @param {Function} callback 
     */
    IsServerInstalled: function (callback) {
        basis.System.PackageManager((packageManager) => {
            if (packageManager === "pacman") {
                basis.System.IsPackageInstalled("nfs-utils", (installed) => {
                    callback(installed);
                });
            } else if (packageManager === "apt") {
                basis.System.IsPackageInstalled("nfs-kernel-server", (installed) => {
                    if (installed === true) {
                        basis.System.IsPackageInstalled("nfs-common", (installed) => {
                            callback(installed);
                        });
                    } else {
                        callback(false);
                    }
                });
            } else {
                //it is assumed it is installed
                callback(true);
            }
        });
    },
    /**
     * @summary Install the server package.
     * @param {Function} callback 
     */
    InstallServer: function (callback) {
        module.exports.IsServerInstalled(installed => {
            if (installed === false) {
                basis.System.PackageManager((packageManager) => {
                    if (packageManager === "pacman") {
                        basis.System.InstallPackage("nfs-utils", (installed) => {
                            callback(installed === true);
                        });
                    } else if (packageManager === "apt") {
                        basis.System.InstallPackage("nfs-kernel-server nfs-common", (installed) => {
                            callback(installed === true);
                        });
                    } else {
                        callback(false);
                    }
                });
            } else {
                callback(false);
            }
        });
    },
    /**
     * @summary NFS service management.
     */
    Service: require('./nfs/service'),
    /**
     * @summary NFS configuration.
     */
    Configuration: require('./nfs/configuration')
};