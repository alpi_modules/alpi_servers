module.exports = {
    Ftp: require('./ftp'),
    Ssh: require('./ssh'),
    Smb: require('./smb'),
    WireGuard: require('./wireguard'),
};